package com.example.my.notifications.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import com.example.my.notifications.Builder.NotificationBuilder;
import com.example.my.notifications.R;

/**
 * Created by 123 on 24.09.2015.
 */
public class ChargeReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        if (level != -1 && scale != -1) {
            float value = level / (float) scale;
            if (value < 30) {
                NotificationBuilder.notifyWithActions(context, R.drawable.notification_template_icon_bg,
                        R.string.app_name, R.string.charge,
                        context.getResources().getInteger(R.integer.charge));
            }
        }
    }
}
