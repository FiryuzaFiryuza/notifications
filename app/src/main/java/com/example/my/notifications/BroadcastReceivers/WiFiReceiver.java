package com.example.my.notifications.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.example.my.notifications.Builder.NotificationBuilder;
import com.example.my.notifications.MainActivity;
import com.example.my.notifications.R;

/**
 * Created by 123 on 24.09.2015.
 */
public class WiFiReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        String name = intent.getStringExtra("send");

        if (name != null && wifiInfo.getSSID().contains(name)) {
            NotificationBuilder.notify(context, R.drawable.notification_template_icon_bg,
                    R.string.app_name,
                    String.format("connection", name),
                    context.getResources().getInteger(R.integer.connected), false);
        }
    }
}
