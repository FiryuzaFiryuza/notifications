package com.example.my.notifications;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.my.notifications.BroadcastReceivers.AppStartReceiver;
import com.example.my.notifications.BroadcastReceivers.ChargeReceiver;
import com.example.my.notifications.BroadcastReceivers.WiFiReceiver;
import com.example.my.notifications.Builder.NotificationBuilder;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    private Button mBtnAppStart;
    private EditText mEtWifi;
    private Button mBtnCheckWifiConnection;
    private Button mBtnStopCatching;
    private Button mBtnNotifyTime;

    private WiFiReceiver wiFiReceiver;
    private ChargeReceiver chargeReceiver;
    private AppStartReceiver appStartReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mBtnAppStart = (Button) findViewById(R.id.btn_start_app);
        mEtWifi = (EditText) findViewById(R.id.et_wifi_name);
        mBtnCheckWifiConnection = (Button) findViewById(R.id.btn_settings);
        mBtnStopCatching = (Button) findViewById(R.id.btn_ok);
        mBtnNotifyTime = (Button) findViewById(R.id.btn_ok);

        wiFiReceiver = new WiFiReceiver();
        chargeReceiver = new ChargeReceiver();
        appStartReceiver = new AppStartReceiver();

        mBtnAppStart.setOnClickListener(this);

        mBtnCheckWifiConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEtWifi.getText().toString() != null && !mEtWifi.getText().toString().equals("")) {
                    LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(wiFiReceiver, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));

                    Intent intent = new Intent(WifiManager.NETWORK_STATE_CHANGED_ACTION);
                    intent.putExtra("message", mEtWifi.getText().toString());
                    LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Enter name", Toast.LENGTH_LONG).show();
                }
            }
        });
        mBtnStopCatching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wiFiReceiver != null) {
                    LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(wiFiReceiver);
                }
            }
        });

        mBtnNotifyTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog dialog = new TimePickerDialog(MainActivity.this,
                        MainActivity.this, 0, 0, true);
                dialog.show();
            }
        });
        registerReceiver(chargeReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        registerReceiver(appStartReceiver, new IntentFilter("app_start"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
       NotificationBuilder.notify(this, R.drawable.notification_template_icon_bg, R.string.app_name,
               R.string.click, getResources().getInteger(R.integer.click), false);

    }

    @Override
    protected void onStop() {
        super.onStop();
        startService(new Intent(this, NotificationService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(chargeReceiver);
        unregisterReceiver(appStartReceiver);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Intent intent = new Intent(this, NotificationService.class);
        intent.putExtra("hours", hourOfDay);
        intent.putExtra("min", minute);
        startService(intent);
    }
}
