package com.example.my.notifications.BroadcastReceivers;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.my.notifications.Builder.NotificationBuilder;
import com.example.my.notifications.MainActivity;
import com.example.my.notifications.R;

/**
 * Created by 123 on 25.09.2015.
 */
public class LoadReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);
        NotificationBuilder.notify(context, R.drawable.notification_template_icon_bg, R.string.app_name,
                R.string.boot, context.getResources().getInteger(R.integer.load), true, pendingIntent);
    }
}
