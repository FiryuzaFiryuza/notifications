package com.example.my.notifications.BroadcastReceivers;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.my.notifications.Builder.NotificationBuilder;
import com.example.my.notifications.MainActivity;
import com.example.my.notifications.R;

/**
 * Created by 123 on 25.09.2015.
 */
public class TimeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);
        String notificationText;
        boolean canCancel;
        if (intent.getAction().equals(context.getResources().getString(R.string.time_action))) {
            int hour = intent.getIntExtra("hours", 0);
            int minute = intent.getIntExtra("min", 0);
            notificationText = String.format(context.getResources().getString(R.string.time_passed), hour, minute);
            canCancel = false;
        } else {
            notificationText = context.getResources().getString(R.string.one_minute_passed);
            canCancel = true;
        }
        NotificationBuilder.notify(context, R.drawable.notification_template_icon_bg,
                R.string.app_name, notificationText,
                context.getResources().getInteger(R.integer.time_passed), canCancel, pendingIntent);
    }
}
