package com.example.my.notifications.Builder;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.support.v7.app.NotificationCompat;

import com.example.my.notifications.R;

import static java.lang.Integer.*;

/**
 * Created by 123 on 24.09.2015.
 */
public class NotificationBuilder {

    public static void notify(Context context, @DrawableRes int icon, String title, String text,
                              int id, boolean autoCancel) {
        getNotificationManager(context).notify(id, createBuilder(context, icon, title, text, id,
                autoCancel).build());
    }

    public static void notify(Context context, @DrawableRes int icon, String title, String text,
                              int id, boolean autoCancel, PendingIntent pendingIntent) {
        getNotificationManager(context).notify(id, createBuilder(context, icon, title, text, id, autoCancel,
                pendingIntent).build());
    }

    public static void notify(Context context, @DrawableRes int icon, @StringRes int title, @StringRes int text,
                              int id, boolean autoCancel) {
        notify(context, icon, context.getResources().getString(title), context.getResources().getString(text),
                context.getResources().getInteger(id), autoCancel);
    }

    public static void notify(Context context, @DrawableRes int icon, @StringRes int title, @StringRes int text,
                              int id, boolean autoCancel, PendingIntent pendingIntent) {
        notify(context, icon, context.getResources().getString(title), context.getResources().getString(text),
                id, autoCancel, pendingIntent);
    }

    public static void notify(Context context, @DrawableRes int icon, @StringRes int title, String text,
                              int id, boolean autoCancel) {
        notify(context, icon, context.getResources().getString(title), text, id, autoCancel);
    }

    public static void notify(Context context, @DrawableRes int icon, @StringRes int title, String text,
                              int id, boolean autoCancel, PendingIntent pendingIntent) {
        notify(context, icon, context.getResources().getString(title), text, id, autoCancel, pendingIntent);
    }

    public static void notifyWithActions(Context context, @DrawableRes int icon, String title, String text, int id) {
        getNotificationManager(context).notify(id, createActionBuilder(context, icon, title, text,
                id).build());
    }

    public static void notifyWithActions(Context context, @DrawableRes int icon, @StringRes int title,
                                         @StringRes int text, @IntegerRes int id) {
        notifyWithActions(context, icon, context.getResources().getString(title), context.getResources().getString(text),
                id);
    }

    private static NotificationCompat.Builder createBuilder(Context context, int icon, String title,
                                                            String text, int id, boolean autoCancel) {
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(autoCancel);
        return builder;
    }

    private static android.support.v4.app.NotificationCompat.Builder createBuilder(Context context, int icon,
                                                                                   String title, String text, int id,
                                                                                   boolean autoCancel, PendingIntent pendingIntent) {
        return createBuilder(context, icon, title, text, id, autoCancel).setContentIntent(pendingIntent);
    }

    private static android.support.v4.app.NotificationCompat.Builder createActionBuilder(Context context, int icon,
                                                                                         String title, String text, int id) {
        Intent okIntent = new Intent("app_start");
        okIntent.putExtra("app_start", id);
        PendingIntent okPendingIntent = PendingIntent.getBroadcast(context, 0, okIntent, 0);
        Intent settings = new Intent(Intent.ACTION_POWER_USAGE_SUMMARY);
        PendingIntent settingsIntent = PendingIntent.getActivity(context, 0, settings, 0);

        return createBuilder(context, icon, title, text, id, true)
                .addAction(R.drawable.notification_template_icon_bg, context.getResources().getString(R.string.ok), okPendingIntent)
                .addAction(R.drawable.notification_template_icon_bg, context.getResources().getString(R.string.action_settings),
                        settingsIntent);
    }

    private static NotificationManager getNotificationManager(Context context) {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }
}
